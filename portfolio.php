<?php

/* Template Name: Portfolio */

get_header();?>

<!-- Hero -->
<section class="jarallax d-flex align-items-stretch min-vh-100 bg-dark mb-2 mb-lg-0 py-5 py-lg-6" data-jarallax data-speed="0.9">
        <div class="jarallax-img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/creative-agency/portfolio/single/hero-bg-6.jpg);"></div>
        <div class="position-relative container d-flex flex-column justify-content-between mt-5 py-3 pt-md-5 py-lg-3F zindex-2">
          <div class="d-flex flex-sm-row flex-column align-items-sm-center justify-content-sm-between">
            <div class="d-flex mt-sm-0 mt-3 ms-sm-0 ms-auto">
              <div class="icon-dropdown icon-dropstart">
                <a class="btn-social bs-solid rounded-circle bs-light" data-bs-toggle="tooltip" data-bs-placement="top" title="Share">
                  <i class="ci-share"></i>
                </a>
                <ul class="icon-drop-menu">
                  <li class="icon-drop-item">
                    <a href="#" class="btn-social bs-solid rounded-circle bs-light">
                      <i class="ci-linkedin"></i>
                    </a>
                  </li>
                  <li class="icon-drop-item">
                    <a href="#" class="btn-social bs-solid rounded-circle bs-light">
                      <i class="ci-twitter"></i>
                    </a>
                  </li>
                  <li class="icon-drop-item">
                    <a href="#" class="btn-social bs-solid rounded-circle bs-light">
                      <i class="ci-facebook"></i>
                    </a>
                  </li>
                </ul>
              </div>
              <a href="#" class="btn-social bs-solid rounded-circle bs-light ms-2" data-bs-toggle="tooltip" data-bs-placement="top" title="Go to project">
                <i class="ci-arrow-share"></i>
              </a>
            </div>
          </div>
          <div class="d-flex flex-column justify-content-between h-100 mt-4 mt-md-5 pt-4"></div>
        </div>
      </section>

      <!-- Project goal -->
      <section class="container py-5 py-lg-1 text-center" id="section-goal">
        <div class="row justify-content-center">
          <div class="col-lg-8 col-md-10 text-left">
            <h1 class="display-1 mb-4 pb-md-3 text-dark massive2">
              PerpetuuMobile
            </h1>
            <p class="lead text-dark">
              We have created new and modern website, something that can match
              their winning personality as well as a custom-tailored and
              high-detailed social media marketing strategy, to boost their
              ever-so-growing presence.
            </p>
          </div>
        </div>
      </section>

      <section class="container py-5 py-lg-6 text-left">
        <div class="row brief">
          <div class="col-lg-8 col-md-10">
            <p class="text-primary number-section">#01</p>
            <h2 class="h6 mb-3 pb-1 massive3 text-dark">The Brief</h2>
            <p class="lead text-dark mb-lg-6 mb-5 pb-lg-4">
              We love a challenge here at INK, so being tasked with creating a
              new website for one of the region's pioneers in security was an
              exciting prospect. <br /><br />
              Create a simple and well optimized website, where individuals and
              companies could simply fill out an application with all their
              security needs, and the team behind Perpetuu Mobile would use
              those information’s to create the perfect package for the
              potential client. <br />
              <br />
              Not only that, the site needed to be multilingual, optimised for
              strong search/localisation performance and built using a cutting
              edge framework all linked to a flexible content management system.
            </p>
          </div>

          <div class="col-lg-4 col-md-10 g-0 brief2">
            <div class="col-12">
              <h3 class="h6 mb-2 text-primary">Industry</h3>
              <ul class="list-unstyled mb-0 fs-sm">
                <li>Security company</li>
              </ul>
            </div>

            <span class="divider-vertical d-sm-block d-none"></span>

            <div class="col-12 pt-3">
              <h3 class="h6 mb-2 text-primary">Involvement</h3>
              <ul class="list-unstyled mb-0 fs-sm">
                <li>Branding</li>
                <li>Digital Marketing</li>
                <li>Web Design & Mobile App</li>
              </ul>
            </div>

            <span class="divider-light d-sm-block d-none"></span>

            <div class="col-12 pt-3 ">
              <h3 class="h6 mb-2 text-primary">Design</h3>
              <ul class="list-unstyled mb-0 fs-sm">
                <li>UI/UX Design</li>
                <li>Art Direction</li>
              </ul>
            </div>

            <span class="divider-vertical d-sm-block d-none"></span>

            <div class="col-3 pt-3">
              <h3 class="h6 mb-2 pb-1 text-primary">Client</h3>
              <img src="<?php echo get_template_directory_uri(); ?>/assets/img/creative-agency/logo/ppm.svg" alt="PerpetuuMobile"/>
            </div>
          </div>
        </div>

        <div class="row justify-content-left">
          <div class="col-lg-6 col-md-10 text-left">
          </div>
        </div>
      </section>

      <div class="container">
        <div class="row">
          <img class="img-fluid w-100" src="<?php echo get_template_directory_uri(); ?>/assets/img/creative-agency/portfolio/single/hero-bg-sec-2.png" alt=""/>
        </div>
      </div>

      <section class="container py-5 py-lg-6 text-left">
        <div class="row brief">
          <div class="col-lg-8 col-md-10">
            <p class="text-primary number-section">#02</p>
            <h2 class="h6 mb-3 pb-1 massive3 text-dark">
              The Development Process
            </h2>
            <p class="lead text-dark mb-lg-6 mb-5 pb-lg-4">
              Clients main focus has always been starting and doing business
              with other domestic and foreign companies in enhancing their
              private security matters. <br />
              Recently, Perpetuu Mobile initialized the idea of creating
              affordable security packages – designed not only for companies and
              businesses, but also on private households. <br /><br />
              Using that idea as a starting point of our web-design adventure,
              our developers, in coordination with the marketing team, created a
              simple and well optimized website, where individuals and companies
              could simply fill out an application with all their security
              needs, and the team behind Perpetuu Mobile would use those
              information’s to create the perfect package for the potential
              client.
            </p>
          </div>
        </div>
      </section>

      <div class="container">
        <div class="row">
          <img class="img-fluid w-100" src="<?php echo get_template_directory_uri(); ?>/assets/img/creative-agency/portfolio/single/hero-bg-sec-3.png" alt=""/>
        </div>
      </div>

      <section class="container py-5 py-lg-6 text-left">
        <div class="row brief">
          <div class="col-lg-8 col-md-10">
            <p class="text-primary number-section">#03</p>
            <h2 class="h6 mb-3 pb-1 massive3 text-dark">Challenge</h2>
            <p class="lead text-dark mb-lg-6 mb-5 pb-lg-4">
              We love a challenge here at INK, so being tasked with creating a
              new website for one of the region's pioneers in security was an
              exciting prospect. <br /><br />
              Create a simple and well optimized website, where individuals and
              companies could simply fill out an application with all their
              security needs, and the team behind Perpetuu Mobile would use
              those information’s to create the perfect package for the
              potential client. <br />
              <br />
              Not only that, the site needed to be multilingual, optimised for
              strong search/localisation performance and built using a cutting
              edge framework all linked to a flexible content management system.
            </p>
          </div>
        </div>
      </section>

	  <div class="container">
		<span class="bg-overlay opacity-25"></span>
		<img src="<?php echo get_template_directory_uri(); ?>/assets/img/creative-agency/portfolio/single/hero-bg-sec-4.png" alt="">
		<div class="container bg-overlay-content py-sm-5 text-center">
		</div>
	  </div>

      <section class="bg-secondary py-lg-6 pt-5 pb-4">
        <div class="container pt-2 pt-lg-0">
          <p class="text-primary number-section">#04</p>
          <h3 class="massive3">The Results</h3>

          <!-- Post image fade in -->
          <div class="row">
            <article class="col-lg-4 image-fade card card-hover shadow" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/creative-agency/portfolio/single/03.jpg); padding-right: 2%;">
              <div class=" image-inner d-flex flex-column justify-content-between h-100 p-3">
                <div class="card-header border-0 pb-0">
                  <ul class="nav nav-muted mb-2 pb-1">
                    <li class="nav-item me-2">
                      <a class="inner-text nav-link d-inline-block me-2 p-0 fs-sm fw-normal" href="#">Marketing</a>
                    </li>
                  </ul>
                </div>
                <div class="card-body pt-0" style="min-height: 15rem">
                  <h3 class="h6 mb-2 pb-1">
                    <a class="inner-text nav-link" href="#">
                      Increase in Lorem Ipsum.
                    </a>
                  </h3>
                </div>
                <div class="card-footer border-0">
                  <div class="h6 mb-0 text-start">
                    <h1 class="massive2">12.14%</h1>
                  </div>
                </div>
              </div>
            </article>
            <article class="col-lg-4 image-fade card card-hover shadow" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/creative-agency/portfolio/single/03.jpg);">
              <div class="image-inner d-flex flex-column justify-content-between h-100 p-3">
                <div class="card-header border-0 pb-0">
                  <ul class="nav nav-muted mb-2 pb-1">
                    <li class="nav-item me-2">
                      <a class="inner-text nav-link d-inline-block me-2 p-0 fs-sm fw-normal" href="#">Marketing</a>
                    </li>
                  </ul>
                </div>
                <div class="card-body pt-0" style="min-height: 15rem">
                  <h3 class="h6 mb-2 pb-1">
                    <a class="inner-text nav-link" href="#">
                      Increase in Lorem Ipsum.
                    </a>
                  </h3>
                </div>
                <div class="card-footer border-0">
                  <div class="h6 mb-0 text-left">
                    <h1 class="massive2">12.14%</h1>
                  </div>
                </div>
              </div>
            </article>
            <article class="col-lg-4 image-fade card card-hover shadow" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/creative-agency/portfolio/single/03.jpg);">
              <div class="image-inner d-flex flex-column justify-content-between h-100 p-3">
                <div class="card-header border-0 pb-0">
                  <ul class="nav nav-muted mb-2 pb-1">
                    <li class="nav-item me-2">
                      <a class="inner-text nav-link d-inline-block me-2 p-0 fs-sm fw-normal" href="#">Marketing</a>
                    </li>
                  </ul>
                </div>
                <div class="card-body pt-0" style="min-height: 15rem">
                  <h3 class="h6 mb-2 pb-1">
                    <a class="inner-text nav-link" href="#">
                      Increase in Lorem Ipsum.
                    </a>
                  </h3>
                </div>
                <div class="card-footer border-0">
                  <div class="h6 mb-0 text-start">
                    <h1 class="massive2">12.14%</h1>
                  </div>
                </div>
              </div>
            </article>
			<div class="row">
				<p class="lead">*Data collected 3 months after the go-live.</p>
			  </div>
          </div>
      </section>

      <!-- Contact links -->
      <section class="container mb-4 mb-sm-5 mt-md-4 mt-lg-0 px-3 py-lg-6 py-5">
        <h2 class="massive">Let's build something amazing <span class="text-primary">together!</span></h2>
      </section>

      <!-- Latest projects -->
      <section class="bg-secondary py-lg-6 pt-5 pb-4">
        <div class="container pt-2 pt-lg-0">
          <p class="text-primary number-section">Enjoy our latest projects</p>

          <div class="mb-lg-5 mb-4 d-flex justify-content-between">
            <h2 class="massive3 h1 mb-0">Selected work</h2>
            <div class="tns-custom-controls tns-controls-inverse mb-md-n4" id="tns-portfolio-controls" tabindex="0">
              <button type="button" data-controls="prev" tabindex="-1">
                <i class="ci-arrow-left"></i>
              </button>
              <button type="button" data-controls="next" tabindex="-1">
                <i class="ci-arrow-right"></i>
              </button>
            </div>
          </div>
        </div>
        <div class="container px-0">
          <!-- Carousel -->
          <div class="tns-carousel-wrapper mt-n2 mb-lg-5 mb-4">
            <div class="tns-carousel-inner" data-carousel-options='{"nav": false, "controlsContainer": "#tns-portfolio-controls", "responsive": {"0": {"items": 1}, "576": {"items": 2}, "768": {"items": 3}}}'>
              <div class="rounded">
                <a class="portfolio-card-scale text-decoration-none mt-3 mx-3" href="portfolio-single-v1.html">
                  <div class="portfolio-card-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/creative-agency/portfolio/single/03.jpg" alt="Card image"/>
                  </div>
                  <div class="portfolio-card-body">
                    <h3 class="portfolio-card-title">
                      Pink Fur Light Bulb Creative Concept
                    </h3>
                    <span class="fs-sm text-muted">Web Design</span>
                  </div>
                </a>
              </div>
              <div class="rounded">
                <a class="portfolio-card-scale text-decoration-none mt-3 mx-3" href="portfolio-single-v1.html">
                  <div class="portfolio-card-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/creative-agency/portfolio/single/03.jpg" alt="Card image"/>
                  </div>
                  <div class="portfolio-card-body">
                    <h3 class="portfolio-card-title">
                      Minimalistic Mobile Interface
                    </h3>
                    <span class="fs-sm text-muted">Mobile Apps</span>
                  </div>
                </a>
              </div>
              <div class="rounded">
                <a class="portfolio-card-scale text-decoration-none mt-3 mx-3" href="portfolio-single-v1.html">
                  <div class="portfolio-card-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/creative-agency/portfolio/single/03.jpg" alt="Card image"/>
                  </div>
                  <div class="portfolio-card-body">
                    <h3 class="portfolio-card-title">Hardcover Book Concept</h3>
                    <span class="fs-sm text-muted">Branding</span>
                  </div>
                </a>
              </div>
              <div class="rounded">
                <a class="portfolio-card-scale text-decoration-none mt-3 mx-3" href="portfolio-single-v1.html">
                  <div class="portfolio-card-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/creative-agency/portfolio/single/03.jpg" alt="Card image"/>
                  </div>
                  <div class="portfolio-card-body">
                    <h3 class="portfolio-card-title">Mobile App Icon Design</h3>
                    <span class="fs-sm text-muted">Mobile App Design</span>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </div>
      </section>
    <?php the_content();?>

<?php get_footer();?>