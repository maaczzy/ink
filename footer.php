<?php wp_footer();?>

<footer class="footer pt-3 bg-dark">
      <div class="container py-lg-5 py-4">
        <div class="row">
          <div class="col-lg-2 col-md-3 col-12 order-md-1 order-2 mb-md-0 mb-4">
            <h3 class="h6 mb-3 text-uppercase text-light">Want to hire us?</h3>
            <ul class="nav nav-light flex-column">
              <li class="nav-item mb-2">
                <a href="tel:(405)555-0128" class="nav-link p-0 fw-normal">
                  <span class="text-light">Call: </span>
                  (405) 555-0128
                </a>
              </li>
              <li class="nav-item mb-2">
                <a href="mailto:hello@example.com" class="nav-link p-0 fw-normal">
                  <span class="text-light">Email: </span>
                  hello@example.com
                </a>
              </li>
            </ul>
          </div>
          <div class="col-lg-2 col-md-3 col-12 order-md-2 order-1 offset-lg-1 mb-md-0 mb-4">
            <h3 class="h6 mb-3 text-uppercase text-light">Quick links</h3>
            <ul class="nav nav-light flex-md-column flex-sm-row flex-column">
              <li class="nav-item mb-2">
                <a href="index.html" class="nav-link me-md-0 me-sm-4 p-0 fw-normal">Homepage</a>
              </li>
              <li class="nav-item mb-2">
                <a href="portfolio.html" class="nav-link me-md-0 me-sm-4 p-0 fw-normal">Portfolio</a>
              </li>
              <li class="nav-item mb-2">
                <a href="about.html" class="nav-link me-md-0 me-sm-4 p-0 fw-normal">About us</a>
              </li>
              <li class="nav-item mb-2">
                <a href="blog.html" class="nav-link me-md-0 me-sm-4 p-0 fw-normal">Blog</a>
              </li>
              <li class="nav-item mb-2">
                <a href="contacts.html" class="nav-link me-md-0 me-sm-4 p-0 fw-normal">Contacts</a>
              </li>
            </ul>
          </div>
          <div class="col-md-3 col-sm-6 order-md-3 order-sm-4 order-3 offset-lg-1 mb-md-0 mb-4">
            <h3 class="h6 mb-3 text-uppercase text-light">Follow us</h3>
            <a href="#" class=" btn-social bs-solid rounded-circle bs-light me-2 mb-2 mt-md-0 mt-sm-1">
              <i class="ci-facebook"></i>
            </a>
            <a href="#" class=" btn-social bs-solid rounded-circle bs-light me-2 mb-2 mt-md-0 mt-sm-1">
              <i class="ci-instagram"></i>
            </a>
            <a href="#" class=" btn-social bs-solid rounded-circle bs-light me-2 mb-2 mt-md-0 mt-sm-1">
              <i class="ci-twitter"></i>
            </a>
            <a href="#" class="btn-social bs-solid rounded-circle bs-light me-2 mb-2 mt-md-0 mt-sm-1">
              <i class="ci-behance"></i>
            </a>
            <a href="#" class="btn-social bs-solid rounded-circle bs-light mb-2 mt-md-0 mt-sm-1">
              <i class="ci-dribbble"></i>
            </a>
          </div>
          <div class="col-md-3 col-sm-6 order-md-4 order-sm-3 order-4 mb-md-0 mb-4">
            <h3 class="h6 mb-3 text-uppercase text-light">Subscribe</h3>
            <form>
              <div class="input-group input-group-light mb-2 pb-1">
                <input class="form-control pe-5 rounded" type="text" placeholder="Email address*"/>
                <i class="ci-arrow-right lead text-light position-absolute top-50 end-0  translate-middle-y mt-n1 me-3">
                </i>
              </div>
              <small class="d-block form-text fs-xs line-height-base text-light"
                >*Subscribe to our newsletter to receive early discount offers,
                updates and new products info.</small>
            </form>
          </div>
        </div>
        <div class="d-flex justify-content-between mt-2 pt-1">
          <div class="text-light">
            <span class="d-block fs-xs mb-1">
              <span class="fs-sm">&copy; </span>
              All rights reserved.
            </span>
            <span class="d-block fs-xs">
              Made with
              <i class="ci-heart mt-n1 mx-1 fs-base text-primary align-middle"></i>
              <a href="https://createx.studio/" class="text-light" target="_blank" rel="noopener noreferrer">by Createx Studio</a>
            </span>
          </div>
          <div class="d-flex align-items-end">
            <span class="d-sm-inline-block d-none text-light fs-sm me-3">Back to top</span>
            <a class="btn-scroll-top position-static rounded-circle" href="#top" data-scroll>
              <i class="btn-scroll-top-icon ci-angle-up"></i>
            </a>
          </div>
        </div>
      </div>
</footer>

</body>
</html>