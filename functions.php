<?php

    function load_stylesheets(){
        
        wp_register_style('bootstrap', get_template_directory_uri() . '/assets/css/bootstrap/bootstrap.min.css', array(), '1.0', 'all');
        wp_enqueue_style('bootstrap');

        wp_register_style('lightgallery', get_template_directory_uri() . '/assets/css/lightgallery/lightgallery.min.css', array(), '1.0', 'all');
        wp_enqueue_style('lightgallery');

        wp_register_style('tiny_slider', get_template_directory_uri() . '/assets/css/tiny-slider/tiny-slider.css', array(), '1.0', 'all');
        wp_enqueue_style('tiny_slider');

        wp_register_style('theme', get_template_directory_uri() . '/style.css', array(), '1.0', 'all');
        wp_enqueue_style('theme');
    }

    add_action('wp_enqueue_scripts', 'load_stylesheets');


    function load_scripts(){

        wp_register_script('bootsrap_js', get_template_directory_uri(). '/assets/js/bootstrap/boostrap.min.js', '', 1, true);
        wp_enqueue_script('bootsrapjs');

        wp_register_script('bootstrap_bundle', get_template_directory_uri(). '/assets/js/bootstrap/bootstrap.bundle.min.js', '', 1, true);
        wp_enqueue_script('bootstrap_bundle');

        wp_register_script('smooth_scroll', get_template_directory_uri(). '/assets/js/smooth-scroll/smooth-scroll.polyfills.min.js', '', 1, true);
        wp_enqueue_script('smooth_scroll');

        wp_register_script('jarallax', get_template_directory_uri(). '/assets/js/jarallax/jarallax.min.js', '', 1, true);
        wp_enqueue_script('jarallax');

        wp_register_script('lightgallery', get_template_directory_uri(). '/assets/js/lightgallery/lightgallery.min.js', '', 1, true);
        wp_enqueue_script('lightgallery');

        wp_register_script('lg_video', get_template_directory_uri(). '/assets/js/lg-video/lg-video.min.js', '', 1, true);
        wp_enqueue_script('lg_video');

        wp_register_script('parallax', get_template_directory_uri(). '/assets/js/parallax/parallax.min.js', '', 1, true);
        wp_enqueue_script('parallax');

        wp_register_script('tiny_slider', get_template_directory_uri(). '/assets/js/tiny-slider/tiny-slider.js', '', 1, true);
        wp_enqueue_script('tiny_slider');

        wp_register_script('themejs', get_template_directory_uri(). '/theme.js', '', 1, true);
        wp_enqueue_script('themejs');
    }

    add_action('wp_enqueue_scripts', 'load_scripts');
?>